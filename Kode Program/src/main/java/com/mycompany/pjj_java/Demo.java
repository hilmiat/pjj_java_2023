package com.mycompany.pjj_java;
class Karyawan{
    String nama;    
//    private String nama;

    int usia;
    double gaji;
    
    //menentukan nama ==> setter
    void setNama(String nama){
        this.nama = nama;
    }
    //membaca nama ===> getter
    String getNama(){
        return this.nama;
    }
    
}
class Perusahaan{
    String nama;
    String alamat;
    Karyawan[] pegawai;
}

public class Demo {

    public static void main(String[] args) {
        Karyawan k1 = new Karyawan();
        k1.nama = "Budi"; //set property nama
        k1.usia = 35;
        k1.gaji = 2000000;
        
        Karyawan k2 = new Karyawan();
        k2.nama = "Amir";
        k2.usia = 25;
        k2.gaji = 8500000;
        
        Karyawan k3 = k1;
        k3.nama = "Chandra";
        
        System.out.println("k1> nama:" + k1.nama +" ,umur:"+k1.usia+" ,gaji:"+k1.gaji); //get value dari property
        System.out.println("k2> nama:" + k2.nama +" ,umur:"+k2.usia+" ,gaji:"+k2.gaji);
        System.out.println("k3> nama:" + k3.nama +" ,umur:"+k3.usia+" ,gaji:"+k3.gaji);
        
        Perusahaan p1 = new Perusahaan();
        p1.nama = "PT.Angin Ribut";
        p1.alamat = "Depok";
        
        //membuat kumpulan object dari Karyawan dan berisi 3 karyawan
        Karyawan[] pegawais = new Karyawan[3];
        pegawais[0] = k1; 
        pegawais[1] = k2;
        
        p1.pegawai = pegawais;
        
        
        
        
    }
    
}
