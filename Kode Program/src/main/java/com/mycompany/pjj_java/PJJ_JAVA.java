package com.mycompany.pjj_java;

public class PJJ_JAVA {

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount();
        BankAccount b2 = new BankAccount();
        BankAccount b3 = b1;

//        BankAccount b4;
//        b4.deposit(300);
//        
        //setor untuk b1
        b1.deposit(99.99);
        b2.deposit(100.50);
        b3.deposit(45.765);

        //akan error jika dibuat private
//        b1.balance = 100000;
//        b1.balance = 0;
//        
        System.out.println("Saldo b1:" + b1.getBalance());
        System.out.println("Saldo b2:" + b2.getBalance());
        System.out.println("Saldo b3:" + b3.getBalance());

    }
}
