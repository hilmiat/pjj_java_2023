
package com.mycompany.pjj_java.day2;
public class ChoiceQuestion extends Question{
    //modifikasi atau menambahkan sesuatu
    protected String[] choices = new String[5];
    
    /**
     * Menambahkan pilihan jawaban
     * @param urutan
     * @param choice
     * @param isCorrect 
     */
    void addChoice(int urutan,String choice,boolean isCorrect){
        choices[urutan] = choice;
        if(isCorrect){
            setAnswer(String.valueOf(urutan));      
//            setAnswer(urutan+""); 
        }
    }
    
    void cetakPilihanJawaban(){
        for(int i=1;i<this.choices.length;i++){
            System.out.println(i+ " > " +this.choices[i]);
        }
    }
    
    //modifikasi method parent
    @Override
    public void cetakSoal(){
        //cetak soal seperti parent classnya
        super.cetakSoal();
        //cetak pilihan
        this.cetakPilihanJawaban();
    }
    
    
    
    
}
