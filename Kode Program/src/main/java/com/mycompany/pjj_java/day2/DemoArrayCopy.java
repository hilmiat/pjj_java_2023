package com.mycompany.pjj_java.day2;

import java.util.Arrays;

public class DemoArrayCopy {
    public static void main(String[] args) {
        String buah[] = {"Mangga","Pisang","Jambu"};
        System.out.println(buah);
        String[] buahJuga = buah;
        System.out.println(buahJuga);
        
        buahJuga[1] = "Pepaya";
        
        cetakArray(buah);
        
        String keranjangBuah[] =  Arrays.copyOf(buah,2);
        
        System.out.println(keranjangBuah);
        
        cetakArray(keranjangBuah);
        keranjangBuah[0] = "Melon";
        cetakArray(keranjangBuah);
        cetakArray(buah);


    }

    private static void cetakArray(String[] ar) {
        System.out.println("------");
        for(String a: ar){
            System.out.println(a);
        }
    }
    
    
}
