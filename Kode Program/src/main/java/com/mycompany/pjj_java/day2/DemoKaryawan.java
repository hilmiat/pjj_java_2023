/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day2;

/**
 *
 * @author hilmiat
 */
public class DemoKaryawan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Karyawan k1 = new Karyawan();
      
       k1.setNama("Budi");
       k1.setNIP(345604343);
       
       k1.cetakDataKaryawan();
       
       KaryawanMagang magang = new KaryawanMagang();
       magang.setNama("Amir");
       magang.cetakDataKaryawan();
       
       
    }
    
}
