package com.mycompany.pjj_java.day2;
public class DemoLoop {

    public static void main(String[] args) {
        int year = 20;
        double RATE = 0.5;
        double balance = 0;
        while(year > 0){
             double interest = balance * 0.5 / 100;
             balance += interest;
             System.out.println(balance);
        }
        
    }
    
}
