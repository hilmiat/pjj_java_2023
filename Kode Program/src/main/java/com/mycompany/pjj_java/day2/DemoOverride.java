
package com.mycompany.pjj_java.day2;

public class DemoOverride {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        Segitiga s1 = new Segitiga();
        System.out.println(s1);
        
        Segitiga s2 = s1;
        Segitiga s3 = (Segitiga) s1.clone();
        
        s1.cetak(1);
    }
    
}

class Segitiga{
    int alas;
    int tinggi;

    public Segitiga() {
    }

    public Segitiga(int alas, int tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

   

    
    
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Segitiga hasilClonning = new Segitiga();
        hasilClonning.alas = this.alas;
        hasilClonning.tinggi = this.tinggi;
        return hasilClonning;    
        
    }
    
    void cetak(){
        System.out.println("Segitiga dengan alas:"+this.alas+",tinggi:"+this.tinggi);
    }
    //overload
    void cetak(String x){
        System.out.println("Segitiga dengan alas:"+this.alas+",tinggi:"+this.tinggi);
    }
    //overload
    void cetak(int x){
        System.out.println("Segitiga dengan alas:"+this.alas+",tinggi:"+this.tinggi);
    }
    
}
