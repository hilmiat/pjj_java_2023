/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day2;

import java.util.Scanner;

/**
 *
 * @author hilmiat
 */
public class DemoQuestion {

    public static void main(String[] args) {
        Question q1 = new Question();
        q1.setText("Siapa penemu java?");
        q1.setAnswer("James Gosling");
        
        //choice question
        ChoiceQuestion q2 = new ChoiceQuestion();
        q2.setText("Siapa presiden ke-3 RI?");
        
        q2.addChoice(1, "Susilo Bambang YUdhoyono", false);
        q2.addChoice(2, "Megawati Soekarnoputri", false);
        q2.addChoice(3, "BJ Habiebie", true);
        q2.addChoice(4, "Joko Widodo", false);


        MultiChoiceQuestion q3 = new MultiChoiceQuestion();
        q3.setText("Sekarang bulan apa?");
        q3.addChoice(1, "Januari", false);
        q3.addChoice(2, "April", true);
        q3.addChoice(3, "Mei", false);
        q3.addChoice(4, "Ramadhan", true);
        
        System.out.println("JAWABAN yang benar:"+q3.getAnswer());
        
        Question[] bankSoal = {q1,q2,q3};
        for(Question q: bankSoal){
            tanyaJawabanUser(q);
        }
        
//        ChoiceQuestion[] bankSoal2 = {q1,q2};
       
    }

    private static void tanyaJawabanUser(Question q1) {
        q1.cetakSoal();
        System.out.print("Jawaban anda:");
        //menggunakan class scanner untuk baca input user
        Scanner scan = new Scanner(System.in);
        //baca jawaban user,lalu simpan dalam variable response
        String response = scan.nextLine();
        System.out.println(q1.cekJawaban(response));
    }
    
    
    
}