package com.mycompany.pjj_java.day2;
public class Karyawan {
    //? Punya apa
    protected String nama;
    private int NIP;

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNIP(int NIP) {
        this.NIP = NIP;
    }

    public String getNama() {
        return nama;
    }

    public int getNIP() {
        return NIP;
    }
    
    
    
    
    
    //? bisa apa
    void cetakDataKaryawan(){
        System.out.println("Nama: "+this.nama);
        System.out.println("NIP: "+this.NIP);
    }

    @Override
    public boolean equals(Object obj) {
        Karyawan pembanding = (Karyawan) obj;
        
        boolean sama = false;
        if(this.getNama().equalsIgnoreCase(pembanding.getNama())){
            sama = true;
        }
        return sama;
    }
    
}
