/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day2;

/**
 *
 * @author hilmiat
 */
public class KaryawanMagang extends Karyawan{
    //sama seperti Karyawan
    //modifikasi method cetak data
    @Override
    void cetakDataKaryawan(){
        System.out.println("Nama: "+this.nama);
        System.out.println("NIP: MAGANG");
    }
}
