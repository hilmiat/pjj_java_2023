/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day2;

/**
 *
 * @author hilmiat
 */
public class MultiChoiceQuestion extends ChoiceQuestion{

    @Override
    void addChoice(int urutan, String choice, boolean isCorrect) {
        //tambahkan pilihan jawaban dalam kumpulan pilihan
         choices[urutan] = choice;
         //jika ditandai benar, maka tambahkan urutan pada jawaban
         if(isCorrect){
             //jawaban benar sebelumnya + " " + urutan pilihan ini
            String jawabanBenarSebelumnya = (getAnswer()==null)?"":getAnswer()+","; 
            setAnswer(jawabanBenarSebelumnya+urutan); 
         }
    }

    @Override
    public void cetakSoal() {
        super.cetakSoal();
        System.out.println("Pisahkan pilihan dengan tanda \",\" jika jawaban lebih dari satu");
    }
    
    
    
}
