
package com.mycompany.pjj_java.day2;
public class Question {
    //punya apa?
    private String text;
    private String answer;
    
    //
    /* in icoment */
    
    /**
     * Set Pertanyaan
     * @param text yang akan jadi pertanyaan
     */
    public void setText(String text) {
        this.text = text;
    }
    
    
    /**
     * Set jawaban
     * @param answer sebagai jawaban pertanyaan
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }
    
    /**
     * Cek jawaban user
     * @param jawabanUser
     * @return 
     */
    public boolean cekJawaban(String jawabanUser){
        //cek apakah jawaban user sama dengan jawaban soal
//        return jawabanUser.equals(this.answer);
        return jawabanUser.equalsIgnoreCase(answer);
    }
    
    public void cetakSoal(){
        System.out.println(this.text);
    }
    
}
