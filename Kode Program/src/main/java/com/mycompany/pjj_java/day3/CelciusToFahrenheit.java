
package com.mycompany.pjj_java.day3;

public class CelciusToFahrenheit implements Convertible{ 
    float suhu;
   
    @Override
    public String satuanAsal() {
        return "Celcius";
    }
    @Override
    public String satuanTujuan() {
        return "Fahrenheit";
    }
    @Override
    public void setNilai(float nilai) {
        this.suhu = nilai;
    }
    @Override
    public float convert() {
        return 9f/5* this.suhu +32;
    }
}
