package com.mycompany.pjj_java.day3;
public interface Convertible {
    public String satuanAsal();
    public String satuanTujuan();
    public void setNilai(float nilai);
    public float convert();
}
