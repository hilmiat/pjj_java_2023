/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day3;

abstract class Animal{
    //nama
    String nama;
    void sleep(){
        System.out.println("Zzzzz");
    }
    abstract void makeSound();
}

class Pig extends Animal{
    @Override
    void makeSound() {
        System.out.println("Pig say: Oink...Oink...");
    }
}

abstract class Semut extends Animal{

}

/**
 *
 * @author hilmiat
 */
public class DemoAbstract {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Animal a = new Pig();
        a.makeSound();
        a.sleep();
        //tidak bisa membuat obj dari class abstract
//      Animal b = new Semut();
        Animal b = new Semut(){
            @Override
            void makeSound() {
                System.out.println("Semut says: owe...owe...");
            } 
        };
        b.makeSound();
        
    }
    
}
