package com.mycompany.pjj_java.day3;
class Lingkaran{
    double radius;

    public Lingkaran() {}

    public Lingkaran(double radius) {
        this.radius = radius;
    }
    
    
    double hitungKeliling(){
        return 3.14 * this.radius* this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    public static void sayHello(){
        System.out.println("Helllo.....");
    }
    
    
}




public class DemoConstructor {
    public static void main(String[] args) {
        
        Lingkaran l1 = new Lingkaran();
        l1.setRadius(14);
        Lingkaran l2 = new Lingkaran(7);
        
        System.out.println("Keliling lingkaran: "+l1.hitungKeliling());
        System.out.println("Keliling lingkaran: "+l2.hitungKeliling());

        Lingkaran.sayHello();
        
    }
}
