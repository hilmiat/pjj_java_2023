/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day3;

import java.util.Scanner;

/**
 *
 * @author hilmiat
 */
public class DemoConverter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan;
        while(true){
            System.out.println("==== Menu ====");
            System.out.println("1. C to F");
            System.out.println("2. Dollar to Rp");
            System.out.println("3. Kurikulum A to B");

            //tambahkan menu lainnya...
            System.out.println("0. Exit");
            scan = new Scanner(System.in);
            int pil = scan.nextInt();
            if(pil==0){
                break;
            }
            
            //list converter
            Convertible[] converter = new Convertible[5];
            converter[1] = new CelciusToFahrenheit();
            converter[2] = new DollarToRupiah();
            converter[3] = new Matakuliah();
            //cetak pesan
            System.out.println("Masukkan nilai "+converter[pil].satuanAsal()+":");
            //baca nilai input dari user
            scan = new Scanner(System.in);
            int nilai = scan.nextInt();
            //set nilai
            converter[pil].setNilai(nilai);
            //konversi
            float hasilKonversi = converter[pil].convert();
            System.out.println("Setara dengan "+converter[pil].satuanTujuan()+":"+hasilKonversi);
        }
           
    }
    
}
