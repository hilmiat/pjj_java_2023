/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day3;

class OuterClass{
    int x = 5;
    
    class InnerClass{
        int y = 3;
        int innerMethod(){
            return x;
        }
    }
}

class Polygon{
    void display(){
        System.out.println("Inside Polygon class");
    }
}


class Mobil{
    String namaMobil;
    //inner class
    class Mesin{
        String namaMesin;
        //dimensi
        
    }
    //static inner class
    static class Ban{
        String nama_ban;
    }
    //anonimous inner class
    Polygon p = new Polygon(){
        @Override
        void display() {
            super.display(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
        }
        
    };

}




/**
 *
 * @author hilmiat
 */
public class DemoInnerClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        OuterClass outer = new OuterClass();
        System.out.println("Nilai X:"+outer.x);
        
        OuterClass.InnerClass inner =  outer.new InnerClass();
        
        System.out.println("Nilai Y:"+inner.y);
                
        System.out.println("Nilai X di dalam innerClass:"+inner.innerMethod());

        Polygon p = new Polygon();
        p.display();
        
        Polygon pmerah = new Polygon(){
            @Override
            void display() {
               System.out.println("Inside RED polygon");
            }
        };
        
        pmerah.display();
        
        Mobil mobil1 = new Mobil();
        mobil1.namaMobil = "Sedan";
        
        Mobil.Mesin mesin = mobil1.new Mesin();
        
        Mobil.Ban ban = new Mobil.Ban();
       
    }
    
}
