/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day3;

interface Animal{
    void makeSound();
    void sleep();
}

class Pig implements Animal{

    @Override
    public void makeSound() {
        System.out.println("Oink.....");
    }

    @Override
    public void sleep() {
        System.out.println("ZZZzzzzzz");
    }

}


interface Measurable{
    double getMeasure();
}

interface Resizeable{
    void resize(int factor);
}

class Persegi implements Measurable,Resizeable{
    double sisi;

    public Persegi(double sisi) {
        this.sisi = sisi;
    }
    
    
    @Override
    public double getMeasure() {
        return this.sisi * this.sisi;
    }

    @Override
    public void resize(int factor) {
        this.sisi *= factor;
    }

}


class Karyawan implements Measurable{
    double gaji;

    public Karyawan(double gaji) {
        this.gaji = gaji;
    }

    @Override
    public double getMeasure() {
        return this.gaji;
    }
    
    
}


/**
 *
 * @author hilmiat
 */
public class DemoInterface {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Animal a = new Pig();
        a.sleep();
        a.makeSound();
        
        Animal b = new Animal(){
            @Override
            public void makeSound() {
               
            }

            @Override
            public void sleep() {
               
            }
        
        };
        
        Measurable[] data = new Measurable[2];
        data[0] = new Karyawan(10);
        data[1] = new Persegi(5);
        
        for(Measurable m: data){
            System.out.println(m.getMeasure());
        }
        
        Persegi p = (Persegi) data[1];
        
        p.resize(2);
        System.out.println(p.getMeasure());
        
        Measurable m1 = new Karyawan(1);
        Measurable m2 = new Persegi(3);
        
        Measurable m3 = m2;
        Persegi m4 = (Persegi) m2;
        Karyawan m5 = (Karyawan) m1;
       
        
    }
    
}
