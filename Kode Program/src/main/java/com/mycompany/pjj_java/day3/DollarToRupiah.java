/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day3;

/**
 *
 * @author hilmiat
 */
public class DollarToRupiah implements Convertible{
     float uang;
    
    @Override
    public String satuanAsal() {
        return "Dollar";
    }

    @Override
    public String satuanTujuan() {
        return "Rupiah";
    }

    @Override
    public void setNilai(float nilai) {
        this.uang = nilai;
    }

    @Override
    public float convert() {
        return this.uang * 15000;
    }
    
}
