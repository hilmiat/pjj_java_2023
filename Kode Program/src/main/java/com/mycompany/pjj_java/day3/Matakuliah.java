/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day3;

import java.io.Serializable;

/**
 *
 * @author hilmiat
 */
public class Matakuliah implements Convertible {
    int sks;
    String TA;

    @Override
    public String satuanAsal() {
        return "Kurikulum A";
    }

    @Override
    public String satuanTujuan() {
        return "Kurikulum B";
    }

    @Override
    public void setNilai(float nilai) {
         
    }

    @Override
    public float convert() {
         return this.sks = (this.sks - 1);
    }
    
    
}
