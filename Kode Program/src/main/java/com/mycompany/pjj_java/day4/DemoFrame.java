/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day4;

import java.awt.Button;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JFrame;


class MyRectangle extends JComponent{

    @Override
    protected void paintComponent(Graphics g) {
        //bikin rectable
        Rectangle box = new Rectangle();
        box.setRect(10,20,100, 50);
        
        g.draw3DRect(5, 5, 100, 50, true);
        
    }
    
}



/**
 *
 * @author hilmiat
 */
public class DemoFrame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame myframe = new JFrame();
        myframe.setTitle("My new Frame");
        myframe.setSize(300, 400);
        
        MyRectangle rect = new MyRectangle();
        myframe.add(rect);
        
        Button tombol = new Button("ini tombol");
        myframe.add(tombol);
        myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
//        myframe.setDefaultCloseOperation(3);

        myframe.setVisible(true);
    }
    
}
