/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day4;

import java.util.Date;

/**
 *
 * @author hilmiat
 */
public class Pegawai {
    private int id;
    private String nama;
    private String email;
    private String alamat;
    private String departemen;
    private int jenisKelamin;
    private Date tglLahir;

    public Pegawai() {
    }

    public Pegawai(int id, String nama, String email, String alamat, String departemen, int jenisKelamin, Date tglLahir) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.alamat = alamat;
        this.departemen = departemen;
        this.jenisKelamin = jenisKelamin;
        this.tglLahir = tglLahir;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDepartemen() {
        return departemen;
    }

    public void setDepartemen(String departemen) {
        this.departemen = departemen;
    }

    public int getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(int jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public Date getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Date tglLahir) {
        this.tglLahir = tglLahir;
    }

    @Override
    public String toString() {
        return "Pegawai{" + "id=" + id + ", nama=" + nama + ", email=" + email + ", alamat=" + alamat + ", departemen=" + departemen + ", jenisKelamin=" + jenisKelamin + ", tglLahir=" + tglLahir + '}';
    }

   
    
    
}
