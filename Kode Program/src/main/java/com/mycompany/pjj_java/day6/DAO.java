
package com.mycompany.pjj_java.day6;

import com.mycompany.pjj_java.day4.Pegawai;
import java.util.ArrayList;

public interface DAO<T,PK> {
    //membaca semua data
    ArrayList<T> getAll();
    
    //menambah data
    boolean insert(T dataBaru);
    
    //update data
    boolean update(T dataUpdate);
    
    //delete
    boolean delete(PK idDelete);
    
    //find by PK
    T getByPK(PK pk);
    
    //search
    ArrayList<T> search(String searchParam);
    
}
