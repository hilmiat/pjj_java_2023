package com.mycompany.pjj_java.day6;

import com.mycompany.pjj_java.day4.Pegawai;
import java.util.ArrayList;

public class DaoPegawai implements DAO<Pegawai, Integer>{
    ArrayList<Pegawai> dataPegawai;

    public DaoPegawai() {
        dataPegawai = new ArrayList<Pegawai>();
    }
    
    
    @Override
    public ArrayList<Pegawai> getAll() {
        return this.dataPegawai;
    }

    @Override
    public boolean insert(Pegawai dataBaru) {
        return this.dataPegawai.add(dataBaru);
    }

    @Override
    public boolean update(Pegawai dataUpdate) {
        //cari data yang id nya sesuai
        for(Pegawai p: dataPegawai){
            if(p.getId()==dataUpdate.getId()){
                //replace data pegawai pada index tersebut
                //index dari data:
                int idx = dataPegawai.indexOf(p);
                //isi dengan data baru
                dataPegawai.add(idx, dataUpdate);
            }
        }
        return true;
    }

    @Override
    public boolean delete(Integer idDelete) {
         for(Pegawai p: dataPegawai){
            if(p.getId()==idDelete){
                //remove data pegawai tersebut
                dataPegawai.remove(p);
            }
        }
         return true;
    }

    @Override
    public Pegawai getByPK(Integer pk) {
        Pegawai pegawaiCari = new Pegawai();
        for(Pegawai p: dataPegawai){
            if(p.getId()==pk){
                pegawaiCari = p;
            }
        }
        return pegawaiCari;
    }

    @Override
    public ArrayList<Pegawai> search(String searchParam) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
 
    
    
}
