/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import com.mycompany.pjj_java.day4.Pegawai;
import java.util.ArrayList;

/**
 *
 * @author hilmiat
 */
public class DaoPegawaiArray implements DAO<Pegawai,Integer>{

    ArrayList<Pegawai> dataPegawai;
    int nextId;

    public DaoPegawaiArray() {
        if(this.dataPegawai==null){
            this.dataPegawai = new ArrayList<Pegawai>();
            this.nextId = 1;
        }
            
    }
    
   
    @Override
    public ArrayList getAll() {
        return this.dataPegawai;
    }

    @Override
    public boolean insert(Pegawai dataBaru) {
        //set id
        dataBaru.setId(this.nextId);
        //increment id
        this.nextId++;
        //tambahkan ke array
        return this.dataPegawai.add(dataBaru);
    }

    @Override
    public boolean update(Pegawai dataUpdate) {
        //cari pegawai dengan id sesuai dengan dataUpdate
        int idupdate = dataUpdate.getId();       
        Pegawai dataLama = this.getByPK(idupdate);
        //id tidak ditemukan
        if(dataLama == null){
            return false;
        }else{
            //replace data pada array
            int index = this.dataPegawai.indexOf(dataLama);
            this.dataPegawai.remove(index);
            
            this.dataPegawai.add(index, dataUpdate);
            return true;
        }
        
    }

    @Override
    public boolean delete(Integer idDelete) {
        //cari pegawai dengan id idDelete
        Pegawai pegawaiDelete = this.getByPK(idDelete);
        //hapus dari data pegawai
        return this.dataPegawai.remove(pegawaiDelete);
    }

    @Override
    public Pegawai getByPK(Integer idCari) {
        for(Pegawai p: dataPegawai){
            if(p.getId() == idCari){
                return p;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Pegawai> search(String searchParam) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
    
}
