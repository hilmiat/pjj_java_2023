/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import com.mycompany.pjj_java.day4.Pegawai;
import com.mycompany.pjj_java.koneksi.KoneksiDB;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author hilmiat
 */
public class DaoPegawaiDB implements DAO<Pegawai,Integer>{
    Connection koneksi;

    public DaoPegawaiDB() {
        //get koneksi
        this.koneksi = new KoneksiDB().getKoneksi();
    }
    
    
    @Override
    public ArrayList getAll() {
        try{
            //query ke database
            //1 siapkan query
            String query = "SELECT * FROM  PEGAWAI";
            //2 buat statement
            Statement stmt = this.koneksi.createStatement();
            //3 eksekusi query -> simpan dalam ResultSet
            ResultSet rs = stmt.executeQuery(query);
            //simpan hasil dalam array of pegawai
            ArrayList<Pegawai> dataPegawai = new ArrayList<Pegawai>();
            while(rs.next()){
                //buat obj pegawai
                Pegawai p = new Pegawai();
                p.setId(rs.getInt("id"));
                p.setNama(rs.getString("nama"));
                p.setEmail(rs.getString("email"));
                p.setJenisKelamin(rs.getInt("jenis_kelamin"));
                p.setDepartemen(rs.getString("departemen"));
                p.setAlamat(rs.getString("alamat"));
                p.setTglLahir(rs.getDate("tgl_lahir"));
                //tambahkan kedalam arraylist
                dataPegawai.add(p);
            }
            return dataPegawai;
        }catch(SQLException ex){
            System.out.println("Kesalahan query:"+ex.getMessage()); 
            return null;
        }
    }
    @Override
    public boolean insert(Pegawai dataBaru) {
        try{
            String query = "INSERT INTO PEGAWAI (nama,email,jenis_kelamin, departemen,tgl_lahir,alamat) values(?,?,?,?,?,?)";
            PreparedStatement ps = this.koneksi.prepareStatement(query);
            ps.setString(1, dataBaru.getNama());
            ps.setString(2, dataBaru.getEmail());
            ps.setInt(3, dataBaru.getJenisKelamin());
            ps.setString(4, dataBaru.getDepartemen());
            long time = dataBaru.getTglLahir().getTime();
            Date tgl = new Date(time);
            ps.setDate(5, tgl);
            ps.setString(6, dataBaru.getAlamat());
            return ps.execute();
        }catch(SQLException ex){
            System.out.println("Kesalahan:"+ex.getMessage());
            return false;
        }
        
    }

    @Override
    public boolean update(Pegawai dataUpdate) {
        try{
            String query = "UPDATE PEGAWAI SET nama=?, email=?, jenis_kelamin=?, departemen=?,tgl_lahir=?,alamat=? where id=?";
            PreparedStatement ps = this.koneksi.prepareStatement(query);
            ps.setString(1, dataUpdate.getNama());
            ps.setString(2, dataUpdate.getEmail());
            ps.setInt(3, dataUpdate.getJenisKelamin());
            ps.setString(4, dataUpdate.getDepartemen());
            long time = dataUpdate.getTglLahir().getTime();
            Date tgl = new Date(time);
            ps.setDate(5, tgl);
            ps.setString(6, dataUpdate.getAlamat());
            ps.setInt(7, dataUpdate.getId());
            return ps.execute();
        }catch(SQLException ex){
            System.out.println("Kesalahan:"+ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean delete(Integer idDelete) {
        try{
            String query = "DELETE FROM PEGAWAI WHERE id=?";
            PreparedStatement ps = this.koneksi.prepareStatement(query);
            ps.setInt(1, idDelete);
            return ps.execute();
        }catch(SQLException e){
            System.out.println("Kesalahan:"+e.getMessage());
            return false;
        }
    }

    @Override
    public Pegawai getByPK(Integer pk) {
        try{
            String query = "SELECT * FROM PEGAWAI WHERE id=?";
            PreparedStatement ps = this.koneksi.prepareStatement(query);
            ps.setInt(1,pk);
            ResultSet rs = ps.executeQuery();
            rs.next();
            Pegawai p = new Pegawai();
            p.setId(rs.getInt("id"));
            p.setNama(rs.getString("nama"));
            p.setEmail(rs.getString("email"));
            p.setJenisKelamin(rs.getInt("jenis_kelamin"));
            p.setDepartemen(rs.getString("departemen"));
            p.setAlamat(rs.getString("alamat"));
            p.setTglLahir(rs.getDate("tgl_lahir"));
            return p;
        }catch(SQLException ex){
            System.out.println("Kesalahan :"+ex.getMessage());
            return null;
        }
    }

    @Override
    public ArrayList<Pegawai> search(String searchParam) {
         try{
            //query ke database
            //1 siapkan query
            String query = "SELECT * FROM  PEGAWAI WHERE nama like '%?%' ";
            //2 buat statement
            PreparedStatement ps = this.koneksi.prepareStatement(query);
            ps.setString(1, searchParam);
            //3 eksekusi query -> simpan dalam ResultSet
            ResultSet rs = ps.executeQuery();
            //simpan hasil dalam array of pegawai
            ArrayList<Pegawai> dataPegawai = new ArrayList<Pegawai>();
            while(rs.next()){
                //buat obj pegawai
                Pegawai p = new Pegawai();
                p.setId(rs.getInt("id"));
                p.setNama(rs.getString("nama"));
                p.setEmail(rs.getString("email"));
                p.setJenisKelamin(rs.getInt("jenis_kelamin"));
                p.setDepartemen(rs.getString("departemen"));
                p.setAlamat(rs.getString("alamat"));
                p.setTglLahir(rs.getDate("tgl_lahir"));
                //tambahkan kedalam arraylist
                dataPegawai.add(p);
            }
            return dataPegawai;
        }catch(SQLException ex){
            System.out.println("Kesalahan query:"+ex.getMessage()); 
            return null;
        }
    }
    
    
    
    
}
