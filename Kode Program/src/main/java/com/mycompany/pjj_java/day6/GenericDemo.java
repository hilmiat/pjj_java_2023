/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import com.mycompany.pjj_java.day4.Pegawai;
import java.util.ArrayList;

class Pair<T,S>{
    T first;
    S second;

    public Pair() {
    }

    public Pair(T first, S second) {
        this.first = first;
        this.second = second;
    }

    
    public <E> String cetakCollection(ArrayList<E> col){
        String data = "";
        for(E c: col){
            data +=c;
        }
        return data;
    }
    
    
    
    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public S getSecond() {
        return second;
    }

    public void setSecond(S second) {
        this.second = second;
    }

}


/**
 *
 * @author hilmiat
 */
public class GenericDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pair pasangan = new Pair("satu","dua");
        Pair<Integer,String> pasangan2 = new Pair<Integer,String>(1,"Andi");
        Pair<Double,String> pasangan3 = new Pair<Double,String>(1.0,"Andi");
       
        Pair test = new Pair(1,1.4);
        test.setFirst("Test");
        test.setFirst(new Pegawai());
        
        ((String)test.getFirst()).toUpperCase();
        
        
       
        
    }
    
}
