/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author hilmiat
 */
public class ListDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //buat obj linkedlist
        LinkedList<String> staff = new LinkedList<String>();
        //menambahkan data
        staff.addLast("Diana"); //D
        staff.addLast("Harry"); //DH
        staff.addLast("Romeo"); //DHR
        staff.addLast("Tom");   //DHRT
        
        System.out.println(staff);
        
        //buat iterator untuk mengakses data (pointer)
        ListIterator<String> iterator = staff.listIterator(); // | DHRT
        //mengunjungi isi dari staff
        iterator.next();                                      // D | HRT
        iterator.next();                                      // DH | RT
        //add data ditengah
        iterator.add("Juliet");                             // DHJ | RT
        iterator.add("Nina");                             // DHJN | RT
        iterator.next();                                    // DHJNR | T
        iterator.remove();                                  // DHJN | T
        
        System.out.println(staff);
    }
    
}
