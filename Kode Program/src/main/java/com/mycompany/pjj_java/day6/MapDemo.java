/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import com.mycompany.pjj_java.day4.Pegawai;
import java.awt.Color;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author hilmiat
 */
public class MapDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Map<String, Color> favoritesColors = new HashMap<String,Color>();
        
        favoritesColors.put("Juliet", Color.blue);
        favoritesColors.put("Romeo", Color.BLACK);
        
        Map<Integer,Pegawai> dataPegawai = new HashMap<Integer,Pegawai>();
        dataPegawai.put(1, new Pegawai(1, "Romeo", "email", "alamat", "departemen", 0, new Date()));
        dataPegawai.put(2, new Pegawai(2, "Juliet", "email", "alamat", "departemen", 1, new Date()));
        
        //mendapatkan pegawai dengan id 1;
        dataPegawai.get(1);
        
        //untuk mendapatkan semua key
        Set<Integer> idsPegawai = dataPegawai.keySet();
        for(int id: idsPegawai){
            System.out.println(dataPegawai.get(id));
        }
        
    }
    
}
