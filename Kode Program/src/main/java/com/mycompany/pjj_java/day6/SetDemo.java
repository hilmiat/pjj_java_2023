/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author hilmiat
 */
public class SetDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set<String> words = new HashSet<String>();
        words.add("Romeo");
        words.add("Juliet");
        words.add("Romeo");
        
        System.out.println(words);
        
    }
    
}
