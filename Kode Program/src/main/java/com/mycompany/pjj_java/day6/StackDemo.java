/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import java.util.Stack;

interface A {}
class B implements A {}
class C implements A {}
class D extends B {}



/**
 *
 * @author hilmiat
 */
public class StackDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Stack<String> myStack = new Stack<>(); 
       
       //menambah data
       myStack.add("Romeo");
       myStack.add("Juliet");
       myStack.add("Tom");
       
        System.out.println(myStack);

       //mendapatkan yang paling atas
       String atas = myStack.pop();
        System.out.println(myStack);
        String atas2 = myStack.peek();
        System.out.println(myStack);
       
    }
    
}
