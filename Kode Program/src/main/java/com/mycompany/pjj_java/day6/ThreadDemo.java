/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.day6;

import java.util.logging.Level;
import java.util.logging.Logger;


class MyThread implements Runnable{
    int jmlLoop;
    int sleepTime;
    String threadName;
    Thread t;

    public MyThread(int jmlLoop, int sleepTime, String threadName) {
        this.jmlLoop = jmlLoop;
        this.sleepTime = sleepTime;
        this.threadName = threadName;
    }
    
   
    @Override
    public void run() {
        //unit ini ingin dijalankan secara paralel
        System.out.println("Starting "+threadName);
        for(int i=0;i<jmlLoop;i++){
            try {
                System.out.println(threadName+" loop ke- "+i);
                Thread.sleep(sleepTime);
            } catch (InterruptedException ex) {
                System.out.println(threadName + " interrrupted ");
            }
        }
        System.out.println(threadName + " selesai ");
    }
    
    public void go(){
        System.out.println("running "+threadName);
        t = new Thread(this);
        t.start();
    }
}





/**
 *
 * @author hilmiat
 */
public class ThreadDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyThread t1 = new MyThread(8, 5000, "T-1");
        MyThread t2 = new MyThread(4, 7000, "T-2");
        
        t1.go();
        t1.go();
        t2.go();

    }
    
}
