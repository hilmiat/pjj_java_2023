/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.pjj_java.koneksi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hilmiat
 */
public class KoneksiDB {
    private Connection koneksi;
    public KoneksiDB() {
        initDB();
          
        
    }
    private void initDB(){
        try {
            Properties prop = new Properties();
            FileInputStream in =new FileInputStream("./config/dbconfig.properties");
            prop.load(in);
            Class.forName(prop.getProperty("driver"));
            this.koneksi = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/aplikasi_pegawai",
                    "user_aplikasi",
                    "password_aplikasi");
        } catch (ClassNotFoundException ex) {
            System.out.println("Gagal load driver");
        } catch (SQLException ex) {
            System.out.println("SQL salah: "+ex.getMessage());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KoneksiDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KoneksiDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getKoneksi() {
        return this.koneksi;
    }
    
    
}
