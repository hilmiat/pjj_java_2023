/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.pjj_java.koneksi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hilmiat
 */
public class TestKoneksi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            //test apakah dependensi sudah berhasil ditambahkan atau belum
            Class.forName("com.mysql.cj.jdbc.Driver");
            //buat koneksi
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/aplikasi_pegawai", "user_aplikasi", "password_aplikasi");
        } catch (ClassNotFoundException ex) {
            System.out.println("Gagal load library");
        } catch (SQLException ex) {
            System.out.println("Gagal koneksi:"+ex.getMessage());
        }
        
    }
    
}
